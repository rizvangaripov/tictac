import java.util.Scanner;

public class TicTac2 {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            String[][] array = new String[3][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    array[i][j] = " ";
                }
            }
            System.out.println("Это игра крестики-нолики. Первыми ходят крестики. Такие правила.");
            System.out.println("Введи сначала координату по горизонтали, затем координату по вертикали, отделив пробелом," +
                    " либо написав строчкой ниже");
            System.out.println("Потом нажми Enter для подтверждения координат");
            outField(array);
            int count = 0;
            boolean isFlag = false;
            while (count < 8) {
                System.out.println("Ход игрока 1");
                String index1 = scanner.next();
                String index2 = scanner.next();
                int index1Num = treatmentIndex1(index1);
                int index2Num = treatmentIndex2(index2);
                array = checkX(array, index1Num, index2Num);
                count++;
                outField(array);
                if (compareFields(array)) {
                    winX();
                    isFlag = true;
                    break;
                }
                System.out.println("Ход игрока 2");
                index1 = scanner.next();
                index2 = scanner.next();
                index1Num = treatmentIndex1(index1);
                index2Num = treatmentIndex2(index2);
                array = check0(array, index1Num, index2Num);
                count++;
                outField(array);
                if (compareFields(array)) {
                    win0();
                    isFlag = true;
                    break;
                }
            }

            if (!isFlag) {
                System.out.println("Ход игрока 1");
                String index1 = scanner.next();
                String index2 = scanner.next();
                int index1Num = treatmentIndex1(index1);
                int index2Num = treatmentIndex2(index2);
                array = checkX(array, index1Num, index2Num);
                outField(array);
                if (compareFields(array)) {
                    winX();
                } else {
                    System.out.println("Ничья");
                }
            }
        }

        static void outField(String[][] array) {
            System.out.println("  0   1   2");
            System.out.println("0 " + array[0][0] + " | " + array[0][1] + " | " + array[0][2]);
            System.out.println(" ---+---+---");
            System.out.println("1 " + array[1][0] + " | " + array[1][1] + " | " + array[1][2]);
            System.out.println(" ---+---+---");
            System.out.println("2 " + array[2][0] + " | " + array[2][1] + " | " + array[2][2]);
        }

        static int treatmentIndex1(String index) {
            Scanner scanner = new Scanner(System.in);
            int indexNum = 0;
            if (index.equals("0") | index.equals("1") | index.equals("2")) {
                indexNum = Integer.parseInt(index);
            } else {
                while (!index.equals("0") & !index.equals("1") & !index.equals("2")) {
                    System.out.println("Ошибка. Введи корректные данные для координаты по горизонтали");
                    index = scanner.nextLine();
                }
                indexNum = Integer.parseInt(index);
            }
            return indexNum;
        }

        static int treatmentIndex2(String index) {
            Scanner scanner = new Scanner(System.in);
            int indexNum = 0;
            if (index.equals("0") | index.equals("1") | index.equals("2")) {
                indexNum = Integer.parseInt(index);
            } else {
                while (!index.equals("0") & !index.equals("1") & !index.equals("2")) {
                    System.out.println("Ошибка. Введи корректные данные для координаты по вертикали");
                    index = scanner.nextLine();
                }
                indexNum = Integer.parseInt(index);
            }
            return indexNum;
        }

        static String[][] checkX(String[][] array, int index1Num, int index2Num) {
            Scanner scanner = new Scanner(System.in);
            if (array[index1Num][index2Num].equals(" ")) {
                array[index1Num][index2Num] = "x";
            } else {
                while (!array[index1Num][index2Num].equals(" ")) {
                    System.out.println("Поле занято, введите другие координаты");
                    String index1 = scanner.next();
                    String index2 = scanner.next();
                    index1Num = treatmentIndex1(index1);
                    index2Num = treatmentIndex2(index2);
                }
                array[index1Num][index2Num] = "x";
            }
            return array;
        }

        static String[][] check0(String[][] array, int index1Num, int index2Num) {
            Scanner scanner = new Scanner(System.in);
            if (array[index1Num][index2Num].equals(" ")) {
                array[index1Num][index2Num] = "0";
            } else {
                while (!array[index1Num][index2Num].equals(" ")) {
                    System.out.println("Поле занято, введите другие координаты");
                    String index1 = scanner.next();
                    String index2 = scanner.next();
                    index1Num = treatmentIndex1(index1);
                    index2Num = treatmentIndex2(index2);
                }
                array[index1Num][index2Num] = "0";
            }
            return array;
        }

        static void winX() {
            System.out.println("        .__                .__                            ____ ");
            System.out.println("__  _  _|__| ____   ______ |  | _____  ___.__. __________/_   |");
            System.out.println("\\ \\/ \\/ /  |/    \\  \\____ \\|  | \\__  \\<   |  |/ __ \\_  __ \\   |");
            System.out.println(" \\     /|  |   |  \\ |  |_> >  |__/ __ \\\\___  \\  ___/|  | \\/   |");
            System.out.println("  \\/\\_/ |__|___|  / |   __/|____(____  / ____|\\___  >__|  |___|");
            System.out.println("                \\/  |__|             \\/\\/         \\/           ");
        }

        static void win0() {
            System.out.println("        .__                .__                            ________  ");
            System.out.println("__  _  _|__| ____   ______ |  | _____  ___.__. ___________\\_____  \\ ");
            System.out.println("\\ \\/ \\/ /  |/    \\  \\____ \\|  | \\__  \\<   |  |/ __ \\_  __ \\/  ____/ ");
            System.out.println(" \\     /|  |   |  \\ |  |_> >  |__/ __ \\\\___  \\  ___/|  | \\/       \\ ");
            System.out.println("  \\/\\_/ |__|___|  / |   __/|____(____  / ____|\\___  >__|  \\_______ \\");
            System.out.println("                \\/  |__|             \\/\\/         \\/              \\/");
        }

        static boolean compareFields(String array[][]) {
            boolean isFlag = false;
            if ((array[0][0].equals(array[0][1]) & array[0][1].equals(array[0][2]) & !array[0][1].equals(" "))
                    | (array[1][0].equals(array[1][1]) & array[1][1].equals(array[1][2]) & !array[1][1].equals(" "))
                    | (array[2][0].equals(array[2][1]) & array[2][1].equals(array[2][2]) & !array[2][1].equals(" "))
                    | (array[0][0].equals(array[1][0]) & array[1][0].equals(array[2][0]) & !array[2][0].equals(" "))
                    | (array[0][1].equals(array[1][1]) & array[1][1].equals(array[2][1]) & !array[2][1].equals(" "))
                    | (array[0][2].equals(array[1][2]) & array[1][2].equals(array[2][2]) & !array[2][2].equals(" "))
                    | (array[0][0].equals(array[1][1]) & array[1][1].equals(array[2][2]) & !array[1][1].equals(" "))
                    | (array[0][2].equals(array[1][1]) & array[1][1].equals(array[2][0]) & !array[0][2].equals(" "))) {
                isFlag = true;
            }
            return isFlag;
        }
}
